const {titleCase}           = require('title-case');
const {AWS, impotent}       = require('@traibe/aws');
const ses                   = new AWS.SES({apiVersion: '2010-12-01'});

const sendEmail = async (to, subject, message, from, format='html') => {
    return new Promise((resolve, reject) => {
        if(impotent)
            return reject({success:false,message:"Impotent email engine, check credentials"});
        if(!['html','text'].includes(format.toLowerCase()))
            return reject({success:false,message:"format must be html or text only"});
        const params = {
            Destination: {
                ToAddresses: [to]
            }, 
            Message: {
                Body: {
                    [titleCase(format)]: {
                        Charset: 'UTF-8',
                        Data: message
                    },
                },
                Subject: {
                    Charset: 'UTF-8',
                    Data: subject
                }
            },
            ReturnPath: from,
            Source: from,
        };
        ses.sendEmail(params, (err, data) => {
            if (err) {
                console.log(err.message);
                return reject({success:false,message:err.message});
            } else {
                console.log(`[${new Date()}]: Email Sent: '${from}' -> '${to}', Subject: '${subject || "<NO SUBJECT>"}', ${JSON.stringify(data)}`);
                return resolve({success:true,message:"Email sent",data})
            }
        });
    })
};

module.exports = {sendEmail};